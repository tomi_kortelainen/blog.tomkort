
// Allow tabs in textareas
function allow_tabs() {
    var textareas = document.getElementsByTagName('textarea');
    var count = textareas.length;
    for(var i=0;i<count;i++){
        textareas[i].onkeydown = function(e){
            if(e.keyCode==9 || e.which==9){
                e.preventDefault();
                var s = this.selectionStart;
                this.value = this.value.substring(0,this.selectionStart) + "    " + this.value.substring(this.selectionEnd);
                this.selectionEnd = s+4;
            }
        }
    }
};

// Bind preview button
function bind_preview_onclick() {
    var btnPreview = document.getElementById('btnPreview');
    if (btnPreview != null) {
        btnPreview.onclick = function () {
            var prevText;
            if (document.getElementById('txtBody') != null) {
                prevText = document.getElementById('txtBody').value;
            } else {
                prevText = document.getElementById('txtComment').value;
            };
            var prevArea = document.getElementById('previewtext');
            var xmlHttp;
            if (window.XMLHttpRequest) {
                xmlHttp=new XMLHttpRequest();
            } else {
                // Old browsers
                xmlHttp=new ActiveXObject('Microsoft.XMLHTTP');
            }
            xmlHttp.onreadystatechange = function () {
                if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                    result = xmlHttp.responseText;
                    console.log(result);
                    prevArea.innerHTML = result;
                }
            }
            param = "prevtext=" + prevText;
            xmlHttp.open('POST', '/preview');
            xmlHttp.setRequestHeader('Content-type',
            'application/x-www-form-urlencoded');
            xmlHttp.setRequestHeader('Content-length', param.length);
            xmlHttp.setRequestHeader('Connection', 'close');
            xmlHttp.send(param);
            return false;
        }
    }
}

// After page has loaded
window.onload = function () {
    allow_tabs();
    bind_preview_onclick();
}