drop table if exists posts;
create table posts (
    id integer primary key autoincrement,
    'date' timestamp not null,
    draft integer not null,
    private integer not null,
    title text not null,
    body text not null,
    tags text
);

drop table if exists comments;
create table comments (
    id integer primary key autoincrement,
    parent integer not null,
    accepted integer not null,
    'date' timestamp not null,
    name text not null,
    body text not null
);
