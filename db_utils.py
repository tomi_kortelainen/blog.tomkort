
import os
import datetime
from sqlite3 import dbapi2 as sqlite
from docutils.core import publish_parts


class Database:
    """
        Utility class to handle database queries to blog posts and comments.

        Nested classes for posts and comments state that these are not to be
        used outside of Database class
    """
    class _Posts:
        _db = None

        def __init__(self, db):
            self._db = db

        def delete(self, id):
            self._db.execute('delete from posts where id = ?', [id])
            self._db.commit()

        def add(self, body, tags, draft=0, private=0):
            title = publish_parts(body, writer_name='html')['title']
            self._db.execute('insert into posts '
                             '(date, draft, private, title, body, tags) '
                             'values (?, ?, ?, ?, ?, ?)',
                             [datetime.datetime.now(), draft, private,
                              title, body, tags])
            self._db.commit()

        def update(self, id, body, tags, draft=0, private=0):
            title = publish_parts(body, writer_name='html')['title']
            if self._db.execute('select draft from posts \
                           where id = ?', [id]).fetchone()['draft'] == 1:
                self._db.execute('update posts set date = ?, draft = ?, '
                                 'private = ?, title = ?, body = ?, tags = ? '
                                 'where id = ?',
                                 [datetime.datetime.now(), draft, private,
                                  title, body, tags, id])
            else:
                self._db.execute('update posts set draft = ?, '
                                 'private = ?, title = ?, body = ?, tags = ? '
                                 'where id = ?',
                                 [draft, private,
                                  title, body, tags, id])
            self._db.commit()

        def get_entries(self, page=1, posts_per_page=10, draft=0, private=0):
            entries = []
            if draft == 1:
                entries_row = self._db.execute('select id, date, title, body, '
                                               'tags from posts '
                                               'where draft = 1 '
                                               'order by date desc limit ?, ?',
                                               [posts_per_page * (page - 1),
                                                posts_per_page]).fetchall()
            else:
                entries_row = self._db.execute('select id, date, title, body, '
                                               'tags from posts '
                                               'where private = ? and draft = ?'
                                               ' order by date desc limit ?, ?',
                                               [private, draft,
                                                posts_per_page * (page - 1),
                                                posts_per_page]).fetchall()
            for entry in entries_row:
                entries.append(dict(zip(entry.keys(), entry)))
                entries[-1]['comments'] = self._db.execute(
                    'select count(*) from comments where parent = ?',
                    [entry['id']]).fetchone()[0]
            return entries

        def get_entries_with_tag(self, tag, admin=False):
            entries = []
            if not admin:
                entries_row = self._db.execute('select id, date, title, body, '
                                               'tags from posts where '
                                               'private = 0 and draft = 0 '
                                               'and tags like ?',
                                               ['%' + tag + '%']).fetchall()
            else:
                entries_row = self._db.execute('select id, date, title, body, '
                                               'tags from posts where '
                                               'tags like ?',
                                               ['%' + tag + '%']).fetchall()
            for entry in entries_row:
                entries.append(dict(zip(entry.keys(), entry)))
                entries[-1]['comments'] = self._db.execute(
                    'select count(*) from comments where parent = ?',
                    [entry['id']]).fetchone()[0]
            return entries

        def get_entry(self, id, admin=False):
            if not admin:
                entry = self._db.execute('select id, date, title, body, '
                                         'tags from posts '
                                         'where id = ? and draft = 0 and '
                                         'private = 0',
                                         [id]).fetchone()
            else:
                entry = self._db.execute('select id, date, title, body, '
                                         'tags from posts '
                                         'where id = ?',
                                         [id]).fetchone()
            return entry

    class _Comments:
        _db = None

        def __init__(self, db):
            self._db = db

        def add(self, parent, name, body, accepted=1):
            cur = self._db.cursor()
            cur.execute('insert into comments (parent, accepted, date, '
                        'name, body) values (?, ?, ?, ?, ?)',
                        [parent, accepted, datetime.datetime.now(), name, body])
            cid = cur.lastrowid
            self._db.commit()
            return cid

        def update(self, id, name, body, accepted=1):
            self._db.execute('update comments set name = ?, body = ?, '
                             'accepted = ? where id = ?',
                             [name, body, accepted, id])
            self._db.commit()

        def delete(self, id):
            self._db.execute('delete from comments where id = ?', [id])
            self._db.commit()

        def get_comments(self, id):
            comments = self._db.execute('select * from comments where parent '
                                        '= ? order by date desc',
                                        [id]).fetchall()
            return comments

        def get_comment(self, id):
            comment = self._db.execute('select * from comments where id = ?',
                                       [id]).fetchone()
            return comment

    """
        Database helper class
    """
    _db = None

    posts = None
    comments = None

    def __init__(self, db_name):
        if os.path.isfile(db_name):
            self._db = sqlite.connect(db_name,
                                      detect_types=sqlite.PARSE_DECLTYPES)
        else:
            self._db = sqlite.connect(db_name,
                                      detect_types=sqlite.PARSE_DECLTYPES)
            self.build()
        self._db.row_factory = sqlite.Row
        self.posts = self._Posts(self._db)
        self.comments = self._Comments(self._db)

    def build(self):
        with open('schema.sql', mode='r') as schema:
            self._db.cursor().executescript(schema.read())
        self._db.commit()



