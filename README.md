# README #

Simplish blog system for my website at [tomkort.net](http://tomkort.net). Uses Flask, Docutils and SQLite.

### Features ###

* Comments
* Commentors are able to edit or delete their comments as long as session is valid
* VERY simple tag system (just text with comma separated tags, searching is done through SQL query with LIKE directive...)
* reStructuredText formatted posts and comments
* Drafts and private posts
* SQLite database
* Comment and post preview.

### Set up ###
Tested on Windows 8.1 in Debug server mode and on Linux with debug server and uwsgi and nginx.

Python 2.7 has been used when developing this.

Install dependencies:
```
#!bash

pip install Flask
pip install docutils
pip install pygments
```

User configuration is located in config.py.

Can be run with python in Debug server mode: 
```
#!bash

python blog.py
```
Flask supports wsgi protocol.
```
#!bash
uwsgi -s /tmp/uwsgi.sock --module blog --callable app
```

## Problems: ##
* Blog post must have title and at least one paragraph. If one is published without those, index page bugs out. Easy to fix, haven't yet though...