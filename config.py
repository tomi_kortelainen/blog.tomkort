class Config:
    db_name = 'blog.db'
    posts_per_page = 10
    username = 'user'
    realname = 'Real Name'
    """
        Password is sha512 encrypted and in this case matches "password".
        Generate your own using pythons hashlib libary like this:
            import hashlib
            hashlib.sha512('password_here').hexdigest()
    """
    password = 'b109f3bbbc244eb82441917ed06d618b9008dd09b3befd1b5e07394c706a8bb\
980b1d7785e5976ec049b46df5f1326af5a2ea6d103fd07c95385ffab0cacbc86'

    """
        This key is used to encode session cookies by Flask. You don't have to
        remember or know it so it could as well be randomly generated.
    """
    secret = "secretsessionkey"
