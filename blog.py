"""

    Copyright (c) 2015 by Tomi Kortelainen

    Simple blog system made with Flask and SQLite. Base model shamelessly taken
    from Flaskr example in Flask repository. Docutils are used for formatting
    reStructuredText to html in posts.

    TODO:
        Soon:
        * Organizing routing better.
        * Too much logic in some templates?
        * Nested if's in edit-route.
        * List of tags
        * Own database table for tags?

        Future:
        * Simple heuristic spam filter for comments.
        * Multiple authors.
        * Setting database and account when first run.

    Licensed under BSD, see LICENSE for details.

"""

import hashlib
import re
from docutils.core import publish_parts, publish_doctree
from flask import Flask, session, request, url_for, render_template, g, \
    redirect, abort
import config
from db_utils import Database

app = Flask(__name__)
cfg = config.Config
app.config.update(dict(
    SECRET_KEY=cfg.secret))

"""

    Utilities
    ---------

"""


def get_database():
    """
        Returns SQLite database object. If database doesn't exist, one is
        created using sql-script in schema.sql.
    """
    if not hasattr(g, 'database'):
        g.database = Database(cfg.db_name)
    return g.database


def get_slug(text, delim='-'):
    """
        Creates url-friendly version of given text.
    """
    special_chars = [',', '.', ':']
    res = text.replace(' ', delim)
    res = re.sub(r"&(.+?);", '', res)
    for char in special_chars:
        res = res.replace(char, '')
    return res


def parse_rst(text, no_title=False):
    """
        Parses given text using Docutils' reStructuredText parser.
    """
    overrides = {'file_insertion_enabled': 0,
                 'raw_enabled': 0,
                 'syntax_highlight': 'short',
                 'report_level': 'quiet'}
    if no_title:
            return publish_parts(text,
                         writer_name='html',
                         settings_overrides=overrides)['body']
    return publish_parts(text,
                         writer_name='html',
                         settings_overrides=overrides)['html_body']


def get_first_paragraph(text):
    """
        Returns first paragraph of given post.
    """
    tree = publish_doctree(text)
    if tree:
        return tree.children[1].rawsource
    else:
        return ""



def do_login(username, password):
    """
        Checks if requested username and password matches the ones in config
        file. Returns True if both matches and False if not.
    """
    if username == cfg.username and \
       hashlib.sha512(password).hexdigest() == cfg.password:
        return True
    return False


@app.before_request
def make_session_permanent():
    session.permanent = False

"""

    Routes
    ------

"""


@app.route('/')
@app.route('/<int:page>')
def index(page=1):
    """
        Route for main page that displays first set of blog posts. Page can be
        optionally given.
    """
    db = get_database()
    entries = db.posts.get_entries(page=page)
    nextpage = False
    if len(entries) == 10:
        nextpage = True
    return render_template('index.html',
                           page=page,
                           entries=entries,
                           subtitle="Blog",
                           split=re.split,
                           parse=parse_rst,
                           nextpage=nextpage,
                           slug=get_slug,
                           firstp=get_first_paragraph)


@app.route('/tag/<tag>')
def show_tag(tag):
    """
        Shows posts tagged with given tag.
    """
    db = get_database()
    if not session.get('loggedin'):
        entries = db.posts.get_entries_with_tag(tag)
    else:
        entries = db.posts.get_entries_with_tag(tag, admin=True)
    return render_template('index.html',
                           entries=entries,
                           subtitle=tag,
                           split=re.split,
                           parse=parse_rst,
                           slug=get_slug,
                           firstp=get_first_paragraph)


@app.route('/drafts/')
def show_drafts():
    """
        Shows drafts only when logged in.
    """
    if session.get('loggedin'):
        db = get_database()
        entries = db.posts.get_entries(draft=1)

        return render_template('index.html',
                               entries=entries,
                               subtitle="Drafts",
                               split=re.split,
                               parse=parse_rst,
                               page=0,
                               slug=get_slug,
                               firstp=get_first_paragraph)
    return redirect(url_for('login'))


@app.route('/private')
@app.route('/private/<int:page>')
def show_private(page=0):
    """
        Shows private posts only when logged in.
    """
    if session.get('loggedin'):
        db = get_database()
        entries = db.posts.get_entries(private=1, draft=0)
        return render_template('index.html',
                               entries=entries,
                               subtitle="Private",
                               parse=parse_rst,
                               split=re.split,
                               page=page,
                               slug=get_slug,
                               firstp=get_first_paragraph)
    return redirect(url_for('login'))


@app.route('/post/<int:postid>')
@app.route('/post/<int:postid>/<title>')
def show_post(postid, title=None):
    """
        Shows post with given id. Optionally title can be given making urls
        more informative.
    """
    db = get_database()
    entry = None
    if not session.get('loggedin'):
        entry = db.posts.get_entry(postid)
        if not entry:
            abort(404)
    else:
        entry = db.posts.get_entry(postid, admin=True)

    comments = db.comments.get_comments(postid)
    return render_template('post.html',
                           entry=entry,
                           subtitle=entry['title'],
                           postid=postid,
                           parse=parse_rst,
                           split=re.split,
                           comments=comments)


@app.route('/login', methods=['GET', 'POST'])
def login():
    """
        Shows login page and handles loggin in with post-method.
    """
    error_msg = None
    if request.method == 'POST':
        if not do_login(request.form['username'], request.form['password']):
            error_msg = 'Username or password is wrong!'
        else:
            session['loggedin'] = True
            return redirect(url_for('index'))
    return render_template('login.html',
                           error=error_msg,
                           subtitle="Log In")


@app.route('/logout')
def logout():
    """
        Logs out.
    """
    session.pop('loggedin', None)
    return redirect(url_for('index'))


@app.route('/edit', methods=['GET', 'POST'])
@app.route('/edit/<int:postid>', methods=['GET', 'POST'])
def edit_post(postid=None):
    """
        If no argument is given, opens editor for new post. Otherwise opens
        given post in edit mode.
    """
    if session.get('loggedin'):
        entry = None
        if request.method == 'POST':
            nexturl = url_for('index')
            db = get_database()
            if request.form['action'] == 'Save draft':
                draft = 1
                nexturl = url_for('show_drafts')
            else:
                draft = 0
            private = request.form.getlist('flags')
            if not private:
                private = 0
            else:
                private = 1
                nexturl = url_for('show_private')
            body = request.form['txtBody']
            tags = request.form['inpTags']
            if postid is None:
                db.posts.add(body, tags, draft=draft, private=private)
            else:
                db.posts.update(postid, body=body, tags=tags, draft=draft,
                                private=private)
            return redirect(nexturl)
        else:
            if postid:
                db = get_database()
                entry = db.posts.get_entry(postid, admin=True)
            return render_template('edit.html',
                                   subtitle="Edit Post",
                                   postid=postid,
                                   entry=entry)
    return redirect(url_for('login'))


@app.route('/comment/<int:postid>', methods=['POST'])
def comment(postid):
    """
        Save comment.
    """
    db = get_database()
    cid = db.comments.add(parent=postid,
                          name=request.form['inpName'],
                          body=request.form['txtComment'],
                          accepted=1)
    if not session.get('commenter'):
        session['commenter'] = True
        session['comments'] = [cid]
    else:
        session['comments'].append(cid)
    return redirect(url_for('show_post', postid=postid))


@app.route('/editcomment/<int:id>', methods=['GET', 'POST'])
def edit_comment(id):
    """
        Comment editor.
    """
    db = get_database()
    if session.get('commenter') and id in session.get('comments'):
        if request.method == 'POST':
            db.comments.update(id, request.form['inpName'],
                               request.form['txtComment'])
            comment = db.comments.get_comment(id)
            return redirect(url_for('show_post', postid=comment['parent']) +
                            "#comment_section")
        else:
            comment = db.comments.get_comment(id)
            return render_template('edit_comment.html', comment=comment)


@app.route('/delcomment/<int:id>')
def delete_comment(id):
    if session.get('loggedin') or (session.get('commenter') and
                                   id in session.get('comments')):
        db = get_database()
        db.comments.delete(id)
    return redirect(request.referrer)


@app.route('/delpost/<int:id>')
def delete_post(id):
    if session['loggedin']:
        db = get_database()
        db.posts.delete(id)
    return redirect(request.referrer)

@app.route('/preview', methods=['POST'])
def preview():
    text = request.form['prevtext']
    res = '<h1>Preview:</h1>' + parse_rst(text)
    return res

if __name__ == '__main__':
    """
        If run directly from python interpreter, starts server in debug mode.
    """
    app.run(debug=True)
